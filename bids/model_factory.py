from factory import django, Faker

from .models import Bid


class BidFactory(django.DjangoModelFactory):
    class Meta:
        model = Bid

    amount = Faker('pyint', min_value=0, max_value=1000)
