import json

from django.test import Client
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient, APIRequestFactory

# initialize the APIClient app
from auctions.model_factory import AuctionFactory
from bids.model_factory import BidFactory
from bids.serializers import BidSerializer
from categories.model_factory import CategoryFactory
from items.model_factory import ItemFactory
from users.model_factory import UserFactory

client = Client()


# Create your tests here.

class BidListDetail(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user_saved = UserFactory.create()
        cls.category_saved = CategoryFactory.create()
        cls.item_saved = ItemFactory.build()
        cls.item_saved.user = cls.user_saved
        cls.item_saved.category = cls.category_saved
        cls.item_saved.save()
        cls.auction_saved = AuctionFactory.build()
        cls.auction_saved.user = cls.user_saved
        cls.auction_saved.item = cls.item_saved
        cls.auction_saved.save()
        cls.bid_saved = BidFactory.build()
        cls.bid_saved.auction = cls.auction_saved
        cls.bid_saved.user = cls.user_saved
        cls.bid_saved.save()
        cls.client = APIClient()
        cls.bid_list = reverse('bid_list')

        cls.factory = APIRequestFactory()
        cls.bid_get_detail = reverse('bid_detail', kwargs={'pk': cls.bid_saved.id})

    def test_get_bid(self):
        # get API response

        # Make request
        response = self.client.get(self.bid_list)

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_bid(self):
        # get API response
        # Prepare data with new auction
        bid = {
            'amount': self.bid_saved.amount + 10,
            'auction': self.bid_saved.auction.id,
            'user': self.bid_saved.user.id,
        }
        # Make request
        response = self.client.post(self.bid_list, bid, format="json")

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Make request
        self.client.force_authenticate(self.user_saved)
        response = self.client.post(self.bid_list, bid, format="json")

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_bid_detail(self):
        # get API response
        # Prepare data with new user

        # Make request
        self.client.force_authenticate(self.user_saved)

        response = self.client.get(self.bid_get_detail, )

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        auction_json = response.content.decode('utf8').replace("'", '"')
        auction_data = json.loads(auction_json)
        bid_serializer = BidSerializer(self.auction_saved, data=auction_data)

        self.assertEqual(
            bid_serializer.initial_data["amount"],
            self.bid_saved.amount,
        )
