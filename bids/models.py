import uuid

from django.db import models

from auctions.models import Auction
from users.models import User


class Bid(models.Model):
    id = models.UUIDField(default=uuid.uuid4, unique=True,
                          primary_key=True, editable=False)
    amount = models.IntegerField(null=False, blank=False)
    auction = models.ForeignKey(Auction, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.auction.name
