from django.db.models import Max
from django.http.response import JsonResponse
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

from auctions.views import check_auctions_owner
from .models import Bid
from .serializers import BidSerializer

NO_PERMISSION = 'You do not have permissions to take this action.'
CREATED_SUCCESSFULLY = 'Created with success.'
DELETED_SUCCESSFULLY = 'Deleted with success.'
NO_ENTITIES_TO_DELETE = 'There is no record available for removal.'
NOT_FOUND = 'There is no record in our database matching your input.'


@swagger_auto_schema(method='post', request_body=BidSerializer)
@api_view(['GET', 'POST', 'DELETE'])
def bid_list(request):
    if request.method == 'GET':
        bids = Bid.objects.filter(user=request.user.id)

        bids_serializer = BidSerializer(bids, many=True)
        return JsonResponse(bids_serializer.data, safe=False)

    elif request.method == 'POST':
        return bid_list_post(request)

    elif request.method == 'DELETE':
        bids = Bid.objects.filter(user=request.user.id)
        if bids.count() == 0:
            return JsonResponse({'message': ('%s' % NO_ENTITIES_TO_DELETE)},
                                status=status.HTTP_404_NOT_FOUND)
        else:
            bids.delete()
            return JsonResponse({'message': ('%s' % DELETED_SUCCESSFULLY)},
                                status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'DELETE'])
def bid_detail(request, pk):
    # find bid by pk (id)
    try:
        bid = Bid.objects.get(id=pk)
    except Bid.DoesNotExist:
        return JsonResponse({'message': ('%s' % NOT_FOUND)},
                            status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        bid_serializer = BidSerializer(bid)
        return JsonResponse(bid_serializer.data)

    elif request.method == 'DELETE':
        bid.delete()
        return JsonResponse({'message': ('%s' % DELETED_SUCCESSFULLY)},
                            status=status.HTTP_204_NO_CONTENT)


def bid_list_post(request):
    bid_data = JSONParser().parse(request)
    bid_serializer = BidSerializer(data=bid_data)
    if bid_serializer.is_valid():
        if bid_serializer.validated_data["user"].id == request.user.id:
            if check_auctions_owner(request.user.id, bid_serializer.validated_data["auction"]):
                if confirm_bid(bid_serializer.validated_data["amount"], bid_serializer.validated_data["auction"]):
                    bid_serializer.save()

                    return JsonResponse({'message': ('%s' % CREATED_SUCCESSFULLY)},
                                        status=status.HTTP_201_CREATED)

                return JsonResponse({'message': 'The current bid is higher!'},
                                    status=status.HTTP_406_NOT_ACCEPTABLE)

            return JsonResponse({'message': 'User cannot bid for his own auction.'},

                                status=status.HTTP_406_NOT_ACCEPTABLE)
        return JsonResponse({'message': 'User cannot bid for other users.'}, status=status.HTTP_400_BAD_REQUEST)
    return JsonResponse(bid_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def confirm_bid(bid, auction):
    bids = Bid.objects.filter(auction=str(auction.id))

    max_current_bid = bids.aggregate(Max('amount'))['amount__max']

    if max_current_bid is None or max_current_bid < bid:
        return True
    if max_current_bid >= bid:
        return False


def get_auction_bids(auction):
    bids = Bid.objects.filter(auction=auction)

    return bids
