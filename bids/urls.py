from django.conf.urls import url

from bids import views

urlpatterns = [
    url(r'^api/bids$', views.bid_list, name='bid_list'),
    url(r'^api/bids/(?P<pk>.*)$', views.bid_detail, name='bid_detail'),
]
