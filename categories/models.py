import uuid

from django.db import models


class Category(models.Model):
    id = models.UUIDField(default=uuid.uuid4, unique=True,
                          primary_key=True, editable=False)
    name = models.CharField(max_length=200, null=False, blank=False,
                            unique=True)

    def __str__(self):
        return self.name
