from django.http.response import JsonResponse
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

from .models import Category
from .serializers import CategorySerializer

NO_PERMISSION = 'You do not have permissions to take this action.'
CREATED_SUCCESSFULLY = 'Created with success.'
DELETED_SUCCESSFULLY = 'Deleted with success.'
NO_ENTITIES_TO_DELETE = 'There is no record available for removal.'
NOT_FOUND = 'There is no record in our database matching your input.'


@swagger_auto_schema(method='post', request_body=CategorySerializer)
@api_view(['GET', 'POST', 'DELETE'])
def category_list(request):
    if request.method == 'GET':
        categories = Category.objects.all()

        categories_serializer = CategorySerializer(categories, many=True)
        return JsonResponse(categories_serializer.data, safe=False)

    elif request.method == 'POST':
        return category_list_post(request)

    elif request.method == 'DELETE':
        if request.user.is_superuser:
            if Category.objects.all().count() == 0:
                return JsonResponse({'message': ('%s' % NO_ENTITIES_TO_DELETE)},
                                    status=status.HTTP_404_NOT_FOUND)
            else:
                Category.objects.all().delete()
                return JsonResponse({'message': ('%s' % DELETED_SUCCESSFULLY)},
                                    status=status.HTTP_201_CREATED)
        else:
            return JsonResponse({'message': NO_PERMISSION},
                                status=status.HTTP_403_FORBIDDEN)


@swagger_auto_schema(method='put', request_body=CategorySerializer)
@api_view(['GET', 'PUT', 'DELETE'])
def category_detail(request, pk):
    try:
        category = Category.objects.get(id=pk)
    except Category.DoesNotExist:
        return JsonResponse({'message': ('%s' % NOT_FOUND)},
                            status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        category_serializer = CategorySerializer(category)
        return JsonResponse(category_serializer.data)
    elif request.method == 'PUT':
        if request.user.is_superuser:
            category_data = JSONParser().parse(request)
            category_serializer = CategorySerializer(category, data=category_data)
            if category_serializer.is_valid():
                category_serializer.save()
                return JsonResponse(category_serializer.data)
            return JsonResponse(category_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return JsonResponse({'message': NO_PERMISSION},
                                status=status.HTTP_403_FORBIDDEN)
    elif request.method == 'DELETE':
        if request.user.is_superuser:
            category.delete()
            return JsonResponse({'message': ('%s' % DELETED_SUCCESSFULLY)},
                                status=status.HTTP_204_NO_CONTENT)
        else:
            return JsonResponse({'message': NO_PERMISSION},
                                status=status.HTTP_403_FORBIDDEN)


def category_list_post(request):
    if request.user.is_superuser:
        category_data = JSONParser().parse(request)
        category_serializer = CategorySerializer(data=category_data)
        if category_serializer.is_valid():
            category_serializer.save()
            return JsonResponse(category_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(category_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    else:
        return JsonResponse({'message': ('%s' % NO_PERMISSION)},
                            status=status.HTTP_403_FORBIDDEN)
