from django.conf.urls import url

from categories import views

urlpatterns = [
    url(r'^api/categories$', views.category_list, name='category_list'),
    url(r'^api/categories/(?P<pk>.*)$', views.category_detail, name='category_detail'),
]
