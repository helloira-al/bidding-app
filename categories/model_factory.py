from factory import django, Faker

from .models import Category


class CategoryFactory(django.DjangoModelFactory):
    class Meta:
        model = Category

    name = Faker('word')
