import json

from django.test import Client
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

# initialize the APIClient app
from categories.model_factory import CategoryFactory
from categories.serializers import CategorySerializer
from users.model_factory import UserFactory

client = Client()


# Create your tests here.

class CategoryList(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user_saved = UserFactory.build()
        cls.user_saved.is_superuser = True
        cls.user_saved.save()
        cls.category_saved = CategoryFactory.create()
        cls.category_not_saved = CategoryFactory.build()
        cls.client = APIClient()
        cls.category_list = reverse('category_list')

    def test_get_category(self):
        # get API response

        # Make request
        response = self.client.get(self.category_list)

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_category(self):
        # get API response
        # Prepare data with new category
        category = {
            'name': self.category_not_saved.name,
        }
        self.client.force_authenticate(self.user_saved)
        # Make request
        response = self.client.post(self.category_list, category, format="json")

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_delete_category(self):
        # get API response
        # Prepare data with new category
        category = {
            'name': self.category_saved.name,
        }

        self.client.force_authenticate(self.user_saved)

        # Make request
        response = self.client.delete(self.category_list, category, format="json")

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class CategoryDetail(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user_saved_second = UserFactory.create()
        cls.user_saved = UserFactory.build()
        cls.user_saved.is_superuser = True
        cls.user_saved.save()
        cls.category_saved = CategoryFactory.create()
        cls.client = APIClient()
        cls.category_get_detail = reverse('category_detail', kwargs={'pk': cls.category_saved.id})

    def test_get_category_detail(self):
        # get API response
        # Prepare data with new user

        # Make request
        response = self.client.get(self.category_get_detail)

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        category_json = response.content.decode('utf8').replace("'", '"')
        category_data = json.loads(category_json)
        category_serializer = CategorySerializer(self.category_saved, data=category_data)

        self.assertEqual(
            category_serializer.initial_data["name"],
            self.category_saved.name,
        )

    def test_put_category_detail(self):
        # get API response
        # Prepare data with new user
        category = {
            'name': self.category_saved.name,
        }
        self.client.force_authenticate(self.user_saved)

        # Make request
        response = self.client.put(self.category_get_detail, category, format="json")

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        category_json = response.content.decode('utf8').replace("'", '"')
        category_data = json.loads(category_json)
        category_serializer = CategorySerializer(self.category_saved, data=category_data)

        self.assertEqual(
            category_serializer.initial_data["name"],
            self.category_saved.name,
        )

    def test_put_category_detail_error(self):
        # get API response
        # Prepare data with new user
        category = {
            'name': self.category_saved.name,
        }
        self.client.force_authenticate(self.user_saved)

        # Make request
        response = self.client.put(self.category_get_detail, category)

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_category_detail(self):
        # get API response
        # Prepare data with new user
        category = {
            'name': self.category_saved.name,
        }
        self.client.force_authenticate(self.user_saved_second)

        # Make request
        response = self.client.delete(self.category_get_detail, category, format="json")

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        self.client.force_authenticate(self.user_saved)

        # Make request
        response = self.client.delete(self.category_get_detail, category, format="json")

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
