from django.contrib.auth.hashers import make_password
from django.http.response import JsonResponse
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

from .models import User
from .serializers import UserSerializer

NO_PERMISSION = 'You do not have permissions to take this action.'
CREATED_SUCCESSFULLY = 'Created with success.'
DELETED_SUCCESSFULLY = 'Deleted with success.'
NO_ENTITIES_TO_DELETE = 'There is no record available for removal.'
NOT_FOUND = 'There is no record in our database matching your input.'


@swagger_auto_schema(method='post', request_body=UserSerializer)
@api_view(['GET', 'POST', 'DELETE'])
def user_list(request):
    if request.method == 'GET':
        users = User.objects.all()

        users_serializer = UserSerializer(users, many=True)

        return JsonResponse(users_serializer.data, safe=False)

    elif request.method == 'POST':
        user_data = JSONParser().parse(request)
        user_serializer = UserSerializer(data=user_data)

        if user_serializer.is_valid():
            user_serializer.validated_data['password'] = make_password(user_serializer.validated_data.get('password'))
            user_serializer.save()

            return JsonResponse({'message': ('%s' % CREATED_SUCCESSFULLY)},
                                status=status.HTTP_201_CREATED)

        return JsonResponse(user_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        if request.user.is_superuser:
            users = User.objects.exclude(id=request.user.id)

            if users.count() == 0:
                return JsonResponse({'message': ('%s' % NO_ENTITIES_TO_DELETE)},
                                    status=status.HTTP_404_NOT_FOUND)
            else:
                users.delete()
                return JsonResponse({'message': ('%s' % DELETED_SUCCESSFULLY)},
                                    status=status.HTTP_204_NO_CONTENT)
        else:
            return JsonResponse({'message': ('%s' % NO_PERMISSION)},
                                status=status.HTTP_403_FORBIDDEN)


@swagger_auto_schema(method='put', request_body=UserSerializer)
@api_view(['GET', 'PUT', 'DELETE'])
def user_detail(request, pk):
    try:
        user = User.objects.get(id=pk)
    except User.DoesNotExist:
        return JsonResponse({'message': ('%s' % NOT_FOUND)},
                            status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        user_serializer = UserSerializer(user)
        return JsonResponse(user_serializer.data)
    elif request.method == 'PUT':
        user_data = JSONParser().parse(request)
        user_serializer = UserSerializer(user, data=user_data)
        if user_serializer.is_valid():
            user_serializer.save()
            return JsonResponse(user_serializer.data)
        return JsonResponse(user_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        if request.user.is_superuser:
            user.delete()
            return JsonResponse({'message': ('%s' % DELETED_SUCCESSFULLY)},
                                status=status.HTTP_204_NO_CONTENT)
        else:
            return JsonResponse({'message': ('%s' % NO_PERMISSION)},
                                status=status.HTTP_403_FORBIDDEN)
