import json

from django.contrib.auth.hashers import make_password
from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

# initialize the APIClient app
from .model_factory import UserFactory
from .models import User
from .serializers import UserSerializer

client = Client()


# Create your tests here.


class UsersCreate(TestCase):
    """ Test module for User views """

    def setUp(self):
        User.objects.create(
            username='test', first_name='Test', last_name='Test', email='test@test.com', password=make_password('test'))
        User.objects.create(
            username='dummy', first_name='Dummy', last_name='Dummy', email='dummy@test.com',
            password=make_password('dummy'))

    def test_create_user(self):
        user_created = User.objects.create_user(email='joe@doe.com', password=make_password('test'))

        self.assertEqual(user_created.username, '')
        self.assertEqual(user_created.first_name, '')
        self.assertEqual(user_created.last_name, '')

    def test_create_superuser(self):
        user_created = User.objects.create_superuser(email='joe@doe.com', password=make_password('test'))

        self.assertEqual(user_created.username, '')
        self.assertEqual(user_created.first_name, '')
        self.assertEqual(user_created.last_name, '')

    def test_get_all_users(self):
        # get API response
        response = client.get(reverse('user_list'))
        # get data from db
        # self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class CreateUser(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user_object = UserFactory.build()
        cls.client = APIClient()
        cls.user_registred = reverse('user_list')

    def test_create_user(self):
        # get API response
        # Prepare data with new user
        user = {
            'username': self.user_object.username,
            'first_name': self.user_object.first_name,
            'last_name': self.user_object.last_name,
            'email': self.user_object.email,
            'password': self.user_object.password,
        }
        # Make request
        response = self.client.post(reverse('user_list'), user, format="json")

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        # Check database
        new_user = User.objects.get(username=self.user_object.username)
        self.assertEqual(
            new_user.first_name,
            self.user_object.first_name,
        )
        self.assertEqual(
            new_user.last_name,
            self.user_object.last_name,
        )


class CreateUserExist(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user_object = UserFactory.build()
        cls.user_second_object = UserFactory.create()
        cls.user_saved = UserFactory.build()
        cls.user_saved.is_superuser = True
        cls.user_saved.save()
        cls.client = APIClient()
        cls.user_list_url = reverse('user_list')

    def test_create_user_exist_error(self):
        # get API response
        # Prepare data with new user
        user = {
            'username': self.user_saved.username,
            'first_name': self.user_saved.first_name,
            'last_name': self.user_saved.last_name,
            'email': self.user_saved.email,
            'password': self.user_saved.password,
        }
        # Make request
        response = self.client.post(self.user_list_url, user, format="json")

        # Check status response
        self.assertEqual(User.objects.count(), 2)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_user(self):
        # get API response
        # Prepare data with new user
        user = {
            'username': self.user_saved.username,
            'first_name': self.user_saved.first_name,
            'last_name': self.user_saved.last_name,
            'email': self.user_saved.email,
            'password': self.user_saved.password,
        }
        self.client.force_authenticate(self.user_saved)

        # Make request
        response = self.client.delete(self.user_list_url, user, format="json")

        # Check status response
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_user_no_exist(self):
        # get API response
        # Prepare data with new user
        user = {
            'username': self.user_saved.username,
            'first_name': self.user_saved.first_name,
            'last_name': self.user_saved.last_name,
            'email': self.user_saved.email,
            'password': self.user_saved.password,
        }

        self.client.force_authenticate(self.user_saved)

        users = User.objects.exclude(id=self.user_saved.id)
        users.delete()

        # Make request
        response = self.client.delete(self.user_list_url, user, format="json")

        # Check status response
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        self.client.force_authenticate(self.user_second_object)
        # Make request
        response = self.client.delete(self.user_list_url, user, format="json")

        # Check status response
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class GetUserDetail(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user_saved = UserFactory.create()
        cls.client = APIClient()
        cls.user_get_detail = reverse('user_detail', kwargs={'pk': cls.user_saved.id})

    def test_get_user_detail(self):
        # get API response
        # Prepare data with new user

        # Make request
        response = self.client.get(self.user_get_detail)

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        user_json = response.content.decode('utf8').replace("'", '"')
        user_data = json.loads(user_json)
        user_serializer = UserSerializer(self.user_saved, data=user_data)

        self.assertEqual(
            user_serializer.initial_data["first_name"],
            self.user_saved.first_name,
        )
        self.assertEqual(
            user_serializer.initial_data["last_name"],
            self.user_saved.last_name,
        )


class GetUserDetailNotExist(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user_object = UserFactory.build()
        cls.client = APIClient()
        cls.user_get_detail = reverse('user_detail', kwargs={'pk': cls.user_object.id})

    def test_get_user_detail_not_exist(self):
        # get API response
        # Prepare data with new user

        # Make request
        response = self.client.get(self.user_get_detail)

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class PutUserDetail(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user_saved = UserFactory.build()
        cls.user_saved.is_superuser = True
        cls.user_saved.save()
        cls.client = APIClient()
        cls.user_get_detail = reverse('user_detail', kwargs={'pk': cls.user_saved.id})

    def test_put_user_detail(self):
        # get API response
        # Prepare data with new user
        user = {
            'username': self.user_saved.username,
            'first_name': self.user_saved.first_name,
            'last_name': self.user_saved.last_name,
            'email': self.user_saved.email,
            'password': self.user_saved.password,
        }
        # Make request
        response = self.client.put(self.user_get_detail, user, format="json")

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        user_json = response.content.decode('utf8').replace("'", '"')
        user_data = json.loads(user_json)
        user_serializer = UserSerializer(self.user_saved, data=user_data)

        self.assertEqual(
            user_serializer.initial_data["first_name"],
            self.user_saved.first_name,
        )
        self.assertEqual(
            user_serializer.initial_data["last_name"],
            self.user_saved.last_name,
        )

    def test_put_user_detail_error(self):
        # get API response
        # Prepare data with new user
        user = {
            'username': self.user_saved.username,
            'first_name': self.user_saved.first_name,
            'last_name': self.user_saved.last_name,
            'email': self.user_saved.email,
            'password': self.user_saved.password,
        }
        # Make request
        response = self.client.put(self.user_get_detail, user)

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_user_detail(self):
        # get API response
        # Prepare data with new user
        user = {
            'username': self.user_saved.username,
            'first_name': self.user_saved.first_name,
            'last_name': self.user_saved.last_name,
            'email': self.user_saved.email,
            'password': self.user_saved.password,
        }
        self.client.force_authenticate(self.user_saved)

        # Make request
        response = self.client.delete(self.user_get_detail, user, format="json")

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
