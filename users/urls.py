from django.conf.urls import url

from users import views

urlpatterns = [
    url(r'^api/users$', views.user_list, name='user_list'),
    url(r'^api/users/(?P<pk>.*)$', views.user_detail, name='user_detail'),

]
