from django.conf.urls import url

from auctions import views

urlpatterns = [
    url(r'^api/auctions$', views.auction_list, name='auction_list'),
    url(r'^api/auctions/(?P<pk>.*)$', views.auction_detail, name='auction_detail'),
]
