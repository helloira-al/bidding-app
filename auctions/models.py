import uuid

from django.db import models
from model_utils import Choices

from items.models import Item
from users.models import User


class Auction(models.Model):
    id = models.UUIDField(default=uuid.uuid4, unique=True,
                          primary_key=True, editable=False)
    name = models.CharField(max_length=200)
    created = models.DateTimeField(auto_now_add=True)
    deadline = models.DateTimeField(null=False, blank=False)
    base = models.IntegerField(null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    STATUS_CHOICES = Choices('open', 'closed')
    status = models.CharField(choices=STATUS_CHOICES, default=STATUS_CHOICES.open, max_length=20)

    def __str__(self):
        return self.name
