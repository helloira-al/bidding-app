from factory import django, Faker

from .models import Auction

AUCTION_VALUES = [x[0] for x in Auction.STATUS_CHOICES]


class AuctionFactory(django.DjangoModelFactory):
    class Meta:
        model = Auction

    name = Faker('first_name')
    created = Faker('date_time')
    deadline = Faker('date_time')
    base = Faker('pyint', min_value=0, max_value=1000)
    status = Faker('random_element', elements=AUCTION_VALUES)
