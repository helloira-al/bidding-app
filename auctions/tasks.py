from datetime import datetime

from django.utils import timezone

from bids.models import Bid
from items.models import Item
from .models import Auction


class Auctions:
    def get_expired_auctions(self):
        auctions = Auction.objects.filter(deadline__lte=datetime.now(timezone.utc), status='open')

        return auctions

    def close_auctions(self, auctions):
        for auction in auctions:
            bids = Bid.objects.filter(auction=auction).order_by('-amount')

            if bids.count() != 0:
                # get highest bid
                bid = Bid.objects.filter(auction=auction).order_by('-amount')[0]

                # change the status of item from 'in auction' to 'sold'
                item = Item.objects.filter(id=auction.id)
                item.status = 'sold'
                item.save()

                # assign the item to the user with the highest bid
                data = {'name': item.name, 'description': item.description, 'user': bid.user,
                        'category': item.category, 'status': 'bought'}
                Item.objects.create(**data)

            else:
                item = auction.item
                item.status = 'none'
                item.save()

            auction.status = 'closed'
            auction.save()
