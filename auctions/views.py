from django.http import HttpResponse
from django.http.response import JsonResponse
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

from items.models import Item
from items.views import confirm_user
from .models import Auction
from .serializers import AuctionSerializer
from .tasks import Auctions

NO_PERMISSION = 'You do not have permissions to take this action.'
CREATED_SUCCESSFULLY = 'Created with success.'
DELETED_SUCCESSFULLY = 'Deleted with success.'
NO_ENTITIES_TO_DELETE = 'There is no record available for removal.'
NOT_FOUND = 'There is no record in our database matching your input.'


@swagger_auto_schema(method='post', request_body=AuctionSerializer)
@api_view(['GET', 'POST', 'DELETE'])
def auction_list(request):
    if request.method == 'GET':
        auctions = Auction.objects.all()

        auctions_serializer = AuctionSerializer(auctions, many=True)
        return JsonResponse(auctions_serializer.data, safe=False)

    elif request.method == 'POST':
        auction_data = JSONParser().parse(request)
        auction_serializer = AuctionSerializer(data=auction_data)
        if auction_serializer.is_valid():
            if confirm_user(auction_serializer.validated_data["user"].id, auction_serializer.validated_data["item"]):
                if confirm_auction(auction_serializer.validated_data["user"].id,
                                   auction_serializer.validated_data["item"]):
                    auction_serializer.save()
                    return JsonResponse({'message': ('%s' % CREATED_SUCCESSFULLY)},
                                        status=status.HTTP_201_CREATED)

                return JsonResponse({'message': 'You cannot create more than one auction.'},
                                    status=status.HTTP_400_BAD_REQUEST)
            return JsonResponse({'message': 'You cannot create auction for this item.'},
                                status=status.HTTP_400_BAD_REQUEST)
        return JsonResponse(auction_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        auctions = Auction.objects.filter(user=request.user.id)
        if auctions.count() == 0:
            return JsonResponse({'message': ('%s' % NO_ENTITIES_TO_DELETE)},
                                status=status.HTTP_404_NOT_FOUND)
        else:
            Auction.objects.filter(user=request.user.id).delete()
            return JsonResponse({'message': ('%s' % DELETED_SUCCESSFULLY)},
                                status=status.HTTP_204_NO_CONTENT)


@swagger_auto_schema(method='put', request_body=AuctionSerializer)
@api_view(['GET', 'PUT', 'DELETE'])
def auction_detail(request, pk):
    # find auction by pk (id)
    try:
        auction = Auction.objects.get(id=pk)
    except Auction.DoesNotExist:
        return JsonResponse({'message': ('%s' % NOT_FOUND)},
                            status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        auction_serializer = AuctionSerializer(auction)
        return JsonResponse(auction_serializer.data)
    elif request.method == 'PUT':
        auction_data = JSONParser().parse(request)
        auction_serializer = AuctionSerializer(auction, data=auction_data)
        if auction_serializer.is_valid():
            auction_serializer.save()
            return JsonResponse(auction_serializer.data)
        return JsonResponse(auction_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        try:
            auction = Auction.objects.get(id=pk, user=request.user.id)
        except Auction.DoesNotExist:
            return JsonResponse({'message': 'You cannot delete auctions you do not own.'},
                                status=status.HTTP_403_FORBIDDEN)

        auction.delete()
        return JsonResponse({'message': ('%s' % DELETED_SUCCESSFULLY)},
                            status=status.HTTP_204_NO_CONTENT)


def check_auctions_owner(user, auction_id):
    auctions = Auction.objects.filter(id=user)
    for auction in auctions:
        if auction_id == auction.id:
            return False
    return True


def check_auctions_owner(user, auction_id):
    auctions = Auction.objects.filter(id=user)
    for auction in auctions:
        if auction_id == auction.id:
            return False
    return True


def confirm_auction(user, item):
    items = Item.objects.filter(user=user)

    if item in items:
        auction = Auction.objects.filter(item=item)

        if auction.count() == 0:
            return True
        else:
            return False


def close_expired_auctions(request):
    expired_auctions = Auctions.get_expired_auctions()
    Auctions.close_auctions(expired_auctions)

    return HttpResponse(status=200)
