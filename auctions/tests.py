import json

from django.test import Client
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient, APIRequestFactory

# initialize the APIClient app
from auctions.model_factory import AuctionFactory
from auctions.models import Auction
from auctions.serializers import AuctionSerializer
from auctions.tasks import Auctions
from categories.model_factory import CategoryFactory
from items.model_factory import ItemFactory
from users.model_factory import UserFactory

client = Client()


# Create your tests here.

class AuctionListDetail(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user_saved = UserFactory.create()
        cls.category_saved = CategoryFactory.create()
        cls.item_saved = ItemFactory.build()
        cls.item_saved.user = cls.user_saved
        cls.item_saved.category = cls.category_saved
        cls.item_saved.save()
        cls.auction_saved = AuctionFactory.build()
        cls.auction_saved.status = 'open'
        cls.auction_saved.user = cls.user_saved
        cls.auction_saved.item = cls.item_saved
        cls.auction_saved.save()
        cls.client = APIClient()
        cls.auction_list = reverse('auction_list')

        cls.factory = APIRequestFactory()
        cls.auction_get_detail = reverse('auction_detail', kwargs={'pk': cls.auction_saved.id})

    def test_get_expired_auctions(self):
        auctions = Auctions.get_expired_auctions(self)

        self.assertEqual(auctions.count(), 1)

    def test_close_auctions(self):
        auctions = [self.auction_saved]
        Auctions.close_auctions(self, auctions)
        auctions = Auction.objects.all()

        self.assertEqual(auctions.count(), 1)
        self.assertEqual(auctions[0].status, 'closed')

    def test_get_auction(self):
        # get API response

        # Make request
        response = self.client.get(self.auction_list)

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_auction(self):
        # get API response
        # Prepare data with new auction
        auction = {
            'name': self.auction_saved.name,
            'created': self.auction_saved.created,
            'deadline': self.auction_saved.deadline,
            'base': self.auction_saved.base,
            'user': self.auction_saved.user.id,
            'item': self.auction_saved.item.id,
            'status': self.auction_saved.status,
        }
        # Make request
        response = self.client.post(self.auction_list, auction, format="json")

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_auction(self):
        # get API response
        # Prepare data with new auction
        auction = {
            'name': self.auction_saved.name,
            'created': self.auction_saved.created,
            'deadline': self.auction_saved.deadline,
            'base': self.auction_saved.base,
            'user': self.auction_saved.user.id,
            'item': self.auction_saved.item.id,
            'status': self.auction_saved.status,
        }

        self.client.force_authenticate(self.user_saved)
        # Make request
        response = self.client.delete(self.auction_list, auction, format="json")

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_get_auction_detail(self):
        # get API response
        # Prepare data with new user

        # Make request
        self.client.force_authenticate(self.user_saved)

        response = self.client.get(self.auction_get_detail, )

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        auction_json = response.content.decode('utf8').replace("'", '"')
        auction_data = json.loads(auction_json)
        auction_serializer = AuctionSerializer(self.auction_saved, data=auction_data)

        self.assertEqual(
            auction_serializer.initial_data["name"],
            self.auction_saved.name,
        )

    def test_put_auction_detail(self):
        # get API response
        # Prepare data with new user
        auction = {
            'name': self.auction_saved.name,
            'created': self.auction_saved.created,
            'deadline': self.auction_saved.deadline,
            'base': self.auction_saved.base,
            'user': self.auction_saved.user.id,
            'item': self.auction_saved.item.id,
            'status': self.auction_saved.status,
        }

        self.client.force_authenticate(self.user_saved)

        response = self.client.put(self.auction_get_detail, auction, format="json", )

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        auction_json = response.content.decode('utf8').replace("'", '"')
        auction_data = json.loads(auction_json)
        auction_serializer = AuctionSerializer(self.auction_saved, data=auction_data)

        self.assertEqual(
            auction_serializer.initial_data["name"],
            self.auction_saved.name,
        )
        self.assertEqual(
            auction_serializer.initial_data["base"],
            self.auction_saved.base,
        )

    def test_put_auction_detail_error(self):
        # get API response
        # Prepare data with new user
        auction = {
            'name': self.auction_saved.name,
            'created': self.auction_saved.created,
            'deadline': self.auction_saved.deadline,
            'base': self.auction_saved.base,
            'user': self.auction_saved.user.id,
            'item': self.auction_saved.item.id,
            'status': self.auction_saved.status,
        }

        self.client.force_authenticate(self.user_saved)
        # Make request
        response = self.client.put(self.auction_get_detail, auction)

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
