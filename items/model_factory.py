from factory import django, Faker

from .models import Item

ITEM_VALUES = [x[0] for x in Item.STATUS_CHOICES]


class ItemFactory(django.DjangoModelFactory):
    class Meta:
        model = Item

    name = Faker('first_name')
    description = Faker('text')
    status = Faker('random_element', elements=ITEM_VALUES)
