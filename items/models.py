import uuid

from django.db import models
from model_utils import Choices

from categories.models import Category
from users.models import User


class Item(models.Model):
    id = models.UUIDField(default=uuid.uuid4, unique=True,
                          primary_key=True, editable=False)
    name = models.CharField(max_length=200)
    description = models.TextField(null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    STATUS_CHOICES = Choices('none', 'in_auction', 'sold', 'bought')
    status = models.CharField(choices=STATUS_CHOICES, default=STATUS_CHOICES.none, max_length=20)

    def __str__(self):
        return self.name
