import json

from django.test import Client
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient, APIRequestFactory

# initialize the APIClient app
from auctions.model_factory import AuctionFactory
from bids.model_factory import BidFactory
from categories.model_factory import CategoryFactory
from items.model_factory import ItemFactory
from items.serializers import ItemSerializer
from users.model_factory import UserFactory

client = Client()


# Create your tests here.

class ItemList(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user_saved = UserFactory.create()
        cls.category_saved = CategoryFactory.create()
        cls.item_saved = ItemFactory.build()
        cls.item_saved.user = cls.user_saved
        cls.item_saved.category = cls.category_saved
        cls.item_saved.save()

        cls.item_not_saved = ItemFactory.build()
        cls.item_not_saved.user = cls.user_saved
        cls.item_not_saved.category = cls.category_saved

        cls.user_second_saved = UserFactory.create()
        cls.category_second_saved = CategoryFactory.create()
        cls.item_second_saved = ItemFactory.build()
        cls.item_second_saved.user = cls.user_saved
        cls.item_second_saved.category = cls.category_saved
        cls.item_second_saved.save()
        cls.auction_second_saved = AuctionFactory.build()
        cls.auction_second_saved.user = cls.user_second_saved
        cls.auction_second_saved.item = cls.item_second_saved
        cls.auction_second_saved.save()

        cls.bid_saved = BidFactory.build()
        cls.bid_saved.auction = cls.auction_second_saved
        cls.bid_saved.user = cls.user_saved
        cls.bid_saved.save()

        cls.client = APIClient()
        cls.item_list = reverse('item_list')

    def test_get_item(self):
        # get API response
        self.client.force_authenticate(self.user_saved)

        # Make request
        response = self.client.get(self.item_list)

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_item(self):
        # get API response
        # Prepare data with new item
        item = {
            'name': self.item_not_saved.name,
            'description': self.item_not_saved.description,
            'user': self.item_not_saved.user.id,
            'category': self.item_not_saved.category.id,
            'status': self.item_not_saved.status,
        }
        self.client.force_authenticate(self.user_saved)

        # Make request
        response = self.client.post(self.item_list, item, format="json")

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_delete_item(self):
        # get API response
        # Prepare data with new item
        item = {
            'name': self.item_saved.name,
            'description': self.item_saved.description,
            'user': self.item_saved.user.id,
            'category': self.item_saved.category.id,
            'status': self.item_saved.status,
        }
        self.client.force_authenticate(self.user_saved)

        # Make request
        response = self.client.delete(self.item_list, item, format="json")

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class ItemDetail(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user_saved = UserFactory.create()
        cls.category_saved = CategoryFactory.create()
        cls.item_saved = ItemFactory.build()
        cls.item_saved.user = cls.user_saved
        cls.item_saved.category = cls.category_saved
        cls.item_saved.save()
        cls.client = APIClient()

        cls.factory = APIRequestFactory()
        cls.item_get_detail = reverse('item_detail', kwargs={'pk': cls.item_saved.id})

    def test_get_item_detail(self):
        # get API response
        # Prepare data with new user

        # Make request
        self.client.force_authenticate(self.user_saved)

        response = self.client.get(self.item_get_detail, )

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        item_json = response.content.decode('utf8').replace("'", '"')
        item_data = json.loads(item_json)
        item_serializer = ItemSerializer(self.item_saved, data=item_data)

        self.assertEqual(
            item_serializer.initial_data["name"],
            self.item_saved.name,
        )

    def test_put_item_detail(self):
        # get API response
        # Prepare data with new user
        item = {
            'name': self.item_saved.name,
            'description': self.item_saved.description,
            'user': self.item_saved.user.id,
            'category': self.item_saved.category.id,
            'status': self.item_saved.status,
        }

        self.client.force_authenticate(self.user_saved)

        response = self.client.put(self.item_get_detail, item, format="json", )

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        item_json = response.content.decode('utf8').replace("'", '"')
        item_data = json.loads(item_json)
        item_serializer = ItemSerializer(self.item_saved, data=item_data)

        self.assertEqual(
            item_serializer.initial_data["name"],
            self.item_saved.name,
        )
        self.assertEqual(
            item_serializer.initial_data["description"],
            self.item_saved.description,
        )

    def test_put_item_detail_error(self):
        # get API response
        # Prepare data with new user
        item = {
            'name': self.item_saved.name,
            'description': self.item_saved.description,
            'user': self.item_saved.user.id,
            'category': self.item_saved.category.id,
            'status': self.item_saved.status,
        }
        self.client.force_authenticate(self.user_saved)
        # Make request
        response = self.client.put(self.item_get_detail, item)

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_item_detail(self):
        # get API response
        # Prepare data with new user
        item = {
            'name': self.item_saved.name,
            'description': self.item_saved.description,
            'user': self.item_saved.user.id,
            'category': self.item_saved.category.id,
            'status': self.item_saved.status,
        }
        self.client.force_authenticate(self.user_saved)
        # Make request
        response = self.client.delete(self.item_get_detail, item)

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
