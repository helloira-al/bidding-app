from django.conf.urls import url

from items import views

urlpatterns = [
    url(r'^api/items$', views.item_list, name='item_list'),
    url(r'^api/items/(?P<pk>.*)$', views.item_detail, name='item_detail'),
]
