from django.http.response import JsonResponse
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

from auctions.models import Auction
from bids.models import Bid
from items.filters import ItemFilter
from items.models import Item
from items.serializers import ItemSerializer

NO_PERMISSION = 'You do not have permissions to take this action.'
CREATED_SUCCESSFULLY = 'Created with success.'
DELETED_SUCCESSFULLY = 'Deleted with success.'
NO_ENTITIES_TO_DELETE = 'There is no record available for removal.'
NOT_FOUND = 'There is no record in our database matching your input.'


@swagger_auto_schema(method='post', request_body=ItemSerializer)
@api_view(['GET', 'POST', 'DELETE'])
def item_list(request):
    if request.method == 'GET':
        return item_list_get(request)

    elif request.method == 'POST':
        return item_list_post(request)

    elif request.method == 'DELETE':
        items = Item.objects.filter(user=request.user.id)
        if items.count() == 0:
            return JsonResponse({'message': ('%s' % NO_ENTITIES_TO_DELETE)},
                                status=status.HTTP_404_NOT_FOUND)
        else:
            Item.objects.filter(user=request.user.id).delete()
            return JsonResponse({'message': ('%s' % DELETED_SUCCESSFULLY)},
                                status=status.HTTP_204_NO_CONTENT)


@swagger_auto_schema(method='put', request_body=ItemSerializer)
@api_view(['GET', 'PUT', 'DELETE'])
def item_detail(request, pk):
    # find item by pk (id)
    try:
        item = Item.objects.get(id=pk, user=request.user.id)
    except Item.DoesNotExist:
        return JsonResponse({'message': ('%s' % NOT_FOUND)},
                            status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        item_serializer = ItemSerializer(item)
        return JsonResponse(item_serializer.data)
    elif request.method == 'PUT':
        item_data = JSONParser().parse(request)
        item_serializer = ItemSerializer(item, data=item_data)
        if item_serializer.is_valid():
            item_serializer.save()
            return JsonResponse(item_serializer.data)
        return JsonResponse(item_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        item.delete()
        return JsonResponse({'message': ('%s' % DELETED_SUCCESSFULLY)},
                            status=status.HTTP_204_NO_CONTENT)


def item_list_get(request):
    user_items = Item.objects.filter(user=request.user.id)
    bids = Bid.objects.filter(user=request.user.id)
    if bids.count() == 0:
        item_filter = ItemFilter(request.GET, queryset=user_items)
    else:
        auctions = []
        for bid in bids:
            auction = Auction.objects.filter(id=bid.auction_id).first()
            if auction.id not in auctions:
                bade_items = Item.objects.filter(id=auction.item_id)
                auctions.append(auction.id)

        item_filter = ItemFilter(request.GET, queryset=user_items.union(bade_items))
    items_serializer = ItemSerializer(item_filter.qs, many=True)
    return JsonResponse(items_serializer.data, safe=False)


def item_list_post(request):
    item_data = JSONParser().parse(request)
    item_serializer = ItemSerializer(data=item_data)
    if item_serializer.is_valid():
        if item_serializer.validated_data['user'].id == request.user.id:
            item_serializer.save()
            return JsonResponse({'message': ('%s' % CREATED_SUCCESSFULLY)},
                                status=status.HTTP_201_CREATED)
        else:
            return JsonResponse({'message': 'You cannot create items for other users.'},
                                status=status.HTTP_403_FORBIDDEN)
    return JsonResponse(item_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def confirm_user(user, item):
    items = Item.objects.filter(user=user)

    if item in items:
        return True
    else:
        return False
