# Bidding App

This repo contains the backend of an application, where users can sell and buy items by putting their products at auction and placing bids at other people's auctions.

### Built with

- Django
- Django Rest Framework


### Functionalities

- Users can enter their own products into the system.
- They can create auctions for their items. They specify the duration of the auction by specifying the ending date.
- They can edit or delete their items.
- Users can bid only at other people's auctions and more than once at the same auction. Their bid must always be greater than the current highest bid placed at that auction.
- There is a view that when gets called, checks all the open auctions for their deadline. If the deadline has passed, the item gets assigned to the user with the highest bid and the auction gets closed.
- Users can retrieve all their items . They can filter them based on their name, category or status ('none', 'in-auction', 'sold', 'bought').
- Users can retrieve the list of all users, but can edit and delete only their own data. The admin(superuser) has permission to delete all users.
- Users can retrieve all the categories, but only the admin is allowed to edit or delete them.

## REST API

The REST API exposing the above functionalities can be accessed from [Swagger Editor](http://localhost:8000/swagger/).

For testing purposes, you can get authorization using the below credentials:

- admin/admin (superuser access)

# Code quality

SonarQube analyzed the project to help out writing cleaner and saver code. Shows the coverage of code lines covered by Python tests.
The image below shows the coverage code, critical and smell bugs present at the project.

![SonarQube](images/2021-11-08_08h20_43.png)

# Docker 

To containerize the bidding system project using Docker images, please follow up the commands described below:

```
# build the image
docker build -t bidding-system .

# create the container at the port 8000
docker run -d --name bidding-system -p 8000:8000 bidding-system
```

